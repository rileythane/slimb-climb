﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEditor;

/*[CustomEditor(typeof(CosmeticConfig))]
public class CosmeticConfigEditor : Editor
{
    public override void OnInspectorGUI()
    {
        CosmeticConfig myScript = (CosmeticConfig)target;
  
        if (GUILayout.Button("Print File Location"))
        {
            myScript.PrintFileLoc();
        }
        DrawDefaultInspector();

    }
}*/

[CreateAssetMenu(fileName = "CosmeticConfig", menuName = "Config/Cosmetic", order = 1)]
public class CosmeticConfig : ScriptableObject
{
    public List<Cosmetics.BasicColourData> basicColour = new List<Cosmetics.BasicColourData>();
    public List<Cosmetics.CosmeticsData> cosmeticsets = new List<Cosmetics.CosmeticsData>();

    public void Save()
    {
        // convert this object to JSON
        string jsonOutput = JsonConvert.SerializeObject(this, Formatting.Indented);

        // write out the data
        StreamWriter sw = new StreamWriter(FilePath);
        sw.Write(jsonOutput);
        sw.Close();

    }
    
    public void PrintFileLoc()
    {
        if(File.Exists(FilePath))
            Debug.Log("File Loaction = " + FilePath);
        else
            Debug.LogWarning("FilePath dosn't exist");

    }

    public string FilePath
    {
        get
        {
            return Path.Combine(Application.persistentDataPath, "cosmetic.json");
        }
    }

    public void Load()
    {
        // read in the data (if present)
        if (File.Exists(FilePath))
        {
            // read the contents of the file
            StreamReader sr = new StreamReader(FilePath);
            string jsonInput = sr.ReadToEnd();
            sr.Close();

            // update this object
            JsonUtility.FromJsonOverwrite(jsonInput, this);
        }
    }
}
