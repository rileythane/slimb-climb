﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameManager gameMan;
    public Missions missionMan;
    public Cosmetics cosmeticMan;
    public bool canDestroy = false,
        canMissionProgress = true,
        isCosmeticMission = false;
    public GameObject player,
        lavaParticle;
    float playerDistance = -7;
    public float timerCount,
        timer;

    // Use this for initialization
    void Start()
    {         
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("PlayerSprite");
        missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();
        cosmeticMan = GameObject.FindGameObjectWithTag("CosmeticsManager").GetComponent<Cosmetics>();
        //CosmeticMissionUpdate();
    }

    // Update is called once per frame
    void Update()
    {
        if (canDestroy == true)
        {
            DeathTimer();
        }
        if (gameObject.transform.position.y - player.gameObject.transform.position.y <= playerDistance)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rigidbody2D rb2 = gameObject.GetComponent<Rigidbody2D>();
            rb2.isKinematic = true;
            rb2.gravityScale = 0;
            rb2.velocity = new Vector3(0, 0, 0);
            gameObject.GetComponent<Collider2D>().enabled = false;

            FindObjectOfType<AudioManager>().Play("coin");

            gameMan.CoinPickUp();

            GameObject playerSPRITE = GameObject.FindGameObjectWithTag("PlayerSprite");

            gameObject.transform.parent = playerSPRITE.gameObject.transform;
            gameObject.transform.position = playerSPRITE.transform.position + new Vector3(0, -0.1f, 0);
            gameObject.transform.rotation = playerSPRITE.transform.rotation;
            gameObject.GetComponentInChildren<Animator>().SetTrigger("Collected");
            timer = timerCount;
            canDestroy = true;
        }

        if (collision.gameObject.CompareTag("Lava"))
        {
            Instantiate(lavaParticle, gameObject.transform);
        }
    }    

    public void DeathTimer()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
