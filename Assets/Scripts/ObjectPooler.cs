﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance;

    private void Awake()
    {
        if (instance!=null)
        {
            DestroyImmediate(this);
        }
        else
        {
            instance = this;
        }
    }

    public int amtToPool;

    public GameObject objectToPool;
    public GameObject[] pooledObjects;
    // Use this for initialization
    void Start()
    {
        pooledObjects = new GameObject[amtToPool];
        for (int i = 0; i < pooledObjects.Length; i++)
        {
            GameObject newPoolObj = Instantiate(objectToPool);
            newPoolObj.SetActive(false);
            pooledObjects[i] = newPoolObj;
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Length; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
                return pooledObjects[i];
        }
        return null;
    }
}
