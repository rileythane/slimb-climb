﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest : MonoBehaviour
{

    GameManager gameManager;

    public Cosmetics cosMan;
    public List<Cosmetics.CosmeticsData> basicCosmetics;

    public GameObject prizeanim;
    public Sprite prize10,
        prize25,
        prize50,
        prize100,
        prize200,
        prizeCos;

    public GameObject chestButton;
    public int keys = 0;

    public int first = 60, second = 40, third = 30, fourth = 20, fifth = 10;

    private int[] weightings;
    public int[] coinRewards;

    // Use this for initialization
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        cosMan = GameObject.FindGameObjectWithTag("CosmeticsManager").GetComponent<Cosmetics>();
        keys = gameManager.chestKeys;
        basicCosmetics = GetList();
        weightings = new int[] { first, second, third, fourth, fifth };
        coinRewards = new int[] { 10, 25, 50, 100, 0 };


        //int lootChosen = RandomiseLootReturn();
    }
    public int RandomiseLootReturn()
    {
        int range = 0;
        for (int i = 0; i < weightings.Length; i++)
        {
            range += weightings[i];
        }

        int rand = Random.Range(0, (int)range);
        int top = 0;

        for (int returnValue = 0; returnValue < weightings.Length; returnValue++)
        {
            top += weightings[returnValue];
            if (rand < top)
            {
                return returnValue;
            }
        }
        return 0;
    }

    public List<Cosmetics.CosmeticsData> GetList()
    {
        List<Cosmetics.CosmeticsData> basicList = new List<Cosmetics.CosmeticsData>();
        foreach (Cosmetics.CosmeticsData cos in cosMan.cosmeticsets)
        {
            if (cos.basicCosmetic)
            {
                if (cos.pieceUnlocked[0] == false)
                {
                    basicList.Add(cos);
                }
            }
        }

        return basicList;
    }

    public int GrabRandom(int min, int max)
    {
        return Random.Range(min, max);
    }

    public void GiveCosmetic()    {

        Cosmetics.CosmeticsData gift = basicCosmetics[GrabRandom(0, basicCosmetics.Count - 1)];

        foreach (Cosmetics.CosmeticsData cosmetic in cosMan.cosmeticsets)
        {
            if (cosmetic.name == gift.name)
            {
                cosmetic.pieceUnlocked[0] = true;
                prizeCos = cosmetic.CostumeSprites[0];
                basicCosmetics.Remove(cosmetic);
            }
        }
        GameObject.FindObjectOfType<Cosmetics>().ActiveConfig.Save();

    }

    public int DeterminePrize(int lootChosen)
    {
        int coinPrize = 0;
        switch (lootChosen)
        {
            default:                
                coinPrize = coinRewards[lootChosen];
                PrizeAnim(prize10);

                break;
            case 1:
                coinPrize = coinRewards[lootChosen];
                PrizeAnim(prize25);
                break;
            case 2:
                coinPrize = coinRewards[lootChosen];
                PrizeAnim(prize50);
                break;
            case 3:
                coinPrize = coinRewards[lootChosen];
                PrizeAnim(prize100);
                break;
            case 4:
                if(basicCosmetics.Count > 0)
                {
                    coinPrize = 0;
                    GiveCosmetic();
                    PrizeAnim(prizeCos);
                    break;
                }
                else
                {
                    coinPrize = 200;
                    PrizeAnim(prize200);
                    break;
                }
        }
        gameManager.currency += coinPrize;
        return coinPrize;
    }

    public void PrizeAnim(Sprite prize)
    {
        Image prizeSPR = prizeanim.GetComponent<Image>();
        prizeSPR.sprite = prize;
        Animator anim = prizeanim.GetComponent<Animator>();
        anim.SetTrigger("Play");
    }
    
}

