﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostSpawner : MonoBehaviour
{

    [System.Serializable]
    public class BoostData
    {
        [Tooltip("Name the boost")]
        public string name;

        [Tooltip("Put the prefab here")]
        public GameObject prefab;

        [Tooltip("Set the min/max for the timer")]
        public float minTimer,
            maxTimer;

        public float currentTimer;
    }

    public List<BoostData> boosts = new List<BoostData>();
    public List<GameObject> goodTiles = new List<GameObject>();
    public Vector3 offset;
    public bool boostActive = false;

    // Update is called once per frame
    void Update()
    {
        
        if (boostActive == false)
        {
            BoostTimer();
        }
        else
        {

        }
    }

    public void BoostTimer()
    {
        foreach(BoostData boost in boosts)
        {
            if(boost.currentTimer >= 0)
            {
                boost.currentTimer -= Time.deltaTime;
            }
            else if(boost.currentTimer < 0)
            {
                SpawnBoost(boost.prefab);
                float rng = Random.Range(boost.minTimer, boost.maxTimer);
                boost.currentTimer = rng;
            }
        }
    }

    public void SpawnBoost(GameObject boost)
    {
        if(goodTiles.Count <= 0)
        {
            return;
        }
        else
        {
            int rng = Random.Range(0, goodTiles.Count);
            var newBoost = Instantiate(boost, goodTiles[rng].gameObject.transform);
            newBoost.transform.position += offset;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GoodTile"))
        {
            goodTiles.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GoodTile"))
        {
            goodTiles.Remove(collision.gameObject);
        }
    }

}

