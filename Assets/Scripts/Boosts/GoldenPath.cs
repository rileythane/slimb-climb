﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenPath : MonoBehaviour
{
    bool pickedUp = false;
    public Collider2D col1,
        col2;
    public Sprite tile;
    public BoostSpawner bstSpawner;
    public float deathTimer;

    // Use this for initialization
    void Start()
    {
        bstSpawner = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<BoostSpawner>();
        col2.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            col1.enabled = false;
            pickedUp = true;
            gameObject.GetComponent<Animator>().SetTrigger("used");
            gameObject.transform.parent = collision.gameObject.transform;
            gameObject.transform.position = collision.gameObject.transform.position;
            col2.enabled = true;
            bstSpawner.boostActive = true;
            StartCoroutine(RunDeathTimer(deathTimer));
        }

        if (collision.gameObject.CompareTag("GoodTile"))
        {
            if(pickedUp == true)
            {
                ParticleSystem partsys = collision.gameObject.GetComponent<ParticleSystem>();
                //SpriteRenderer spr = collision.gameObject.GetComponentInChildren<SpriteRenderer>();
                //spr.sprite = tile;
                Animator t = collision.gameObject.GetComponent<Animator>();                
                t.SetTrigger("GoldenPath");
                partsys.Play();
            }
        }
    }

    IEnumerator RunDeathTimer(float timer)
    {
        yield return new WaitForSeconds(timer);
        bstSpawner.boostActive = false;
        Destroy(gameObject);
    }
}
