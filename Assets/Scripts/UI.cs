﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public GameManager gameMan;
    public Missions missionMan;
    public Cosmetics cosmeticsMan;

    public GameObject gameOverUI,
        chest,
        pauseMenu,
        pausePlayButton;

    public List<GameObject> gameUI = new List<GameObject>();

    public Text currentScore,
        highscore,
        currentCoins,
        GOCoins,
        GOScore,
        heightMission,
        coinMission,
        boostMission,
        countDown;

    public GameObject CoinBar,
        HeightBar,
        BoostBar;

    // Use this for initialization
    void Start()
    {
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        cosmeticsMan = GameObject.FindGameObjectWithTag("CosmeticsManager").GetComponent<Cosmetics>();
        gameOverUI = GameObject.FindGameObjectWithTag("GameOverUI");
        gameOverUI.SetActive(false);
        gameMan.score = 0;
        if (highscore && currentCoins)
        {
            string highScoreText = gameMan.highScore.ToString();
            UpdateGameUI(highScoreText, highscore);
            string currencyText = gameMan.currency.ToString();
            UpdateGameUI(currencyText, currentCoins);
        }
    }

    public void Activate(GameObject toActivate)
    {
        toActivate.SetActive(true);
    }

    public void DeActivate(GameObject toDeactivate)
    {
        toDeactivate.SetActive(false);
    }

    public void UpdateGameUI(string updateVar, Text textToUpdate)
    {
        textToUpdate.text = updateVar;
    }

    public void UpdateMissionUI(GameObject toUpdate, float progress, float goal)
    {
        ProgressBar pBar = toUpdate.GetComponent<ProgressBar>();
        float progressPercent = progress / goal;
        pBar.barDisplay = progressPercent;
    }

    public void UpdateGameOverUI()
    {
        GOCoins.text = gameMan.currency.ToString();
        //string scoretext = ("YOU CLIMBED" + " " + gameMan.score + " " + "TILES");
        //GOScore.text = scoretext;
        if (!missionMan)
            missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();

        if (gameMan.heightProgress >= missionMan.HeightMissions[missionMan.currentHeightMission].goal)
            heightMission.text = ("Collect Reward");
        else heightMission.text = ("Tiles Jumped " + gameMan.heightProgress + "/" + missionMan.HeightMissions[missionMan.currentHeightMission].goal);

        if (gameMan.boostProgress >= missionMan.BoostMissions[missionMan.currentBoostMission].goal)
            boostMission.text = ("Collect Reward");
        else boostMission.text = ("Boosts Used " + gameMan.boostProgress + "/" + missionMan.BoostMissions[missionMan.currentBoostMission].goal);

        /* if (gameMan.canProgressCosmeticMission == true)
         {
             if (gameMan.cosmeticProgress >= cosmeticsMan.cosmeticsets[cosmeticsMan.currentSelectionCostume].pieceCost[3])
                 coinMission.text = ("Mission Complete");
             else coinMission.text = ("Collectables Collected " + cosmeticsMan.cosmeticsets[cosmeticsMan.currentSelectionCostume].missionProgress + "/" + cosmeticsMan.cosmeticsets[cosmeticsMan.currentSelectionCostume].pieceCost[3]);
         }
         else
         */
        if (gameMan.coinProgress >= missionMan.CoinMissions[missionMan.currentCoinMission].goal)
            coinMission.text = ("Collect Reward");
        else coinMission.text = ("Coins Collected " + gameMan.coinProgress + "/" + missionMan.CoinMissions[missionMan.currentCoinMission].goal);

    }

    public void UpdateCosmeticsUI()
    {

    }
}
