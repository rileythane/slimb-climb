﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{

    public GameObject player,
        lava;       //Public variable to store a reference to the player game object

    public Vector3 offset;         //Private variable to store the offset distance between the player and camera

    public float dist,
        speed;

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - player.transform.position;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        float step = speed * Time.deltaTime;
        dist = Vector3.Distance(player.transform.position, lava.transform.position);
        offset = new Vector3(0, (dist - (dist * 0.5f) - 2), 1);
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, (player.transform.position + -offset), step);

    }
}