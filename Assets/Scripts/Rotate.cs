﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float speed = 100;

    public GameObject coin;

    public UI ui;

    private void Start()
    {
        ui = FindObjectOfType<UI>();
        if (!coin)
            coin = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        coin.transform.Rotate(0, Time.deltaTime * speed, 0);
        if (speed > 100)
            speed -= 10;
    }

    public void CoinClick()
    {
        speed += 500;
        if (speed > 2000)
            speed = 2000;
        if (ui)
            ui.UpdateGameOverUI();
    }
}
