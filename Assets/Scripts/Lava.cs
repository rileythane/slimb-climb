﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{

    public float timer,
        speed,
        speedReset;

    public bool move = false,
        gamestart = true;

    public Vector3 distance;

    public GameManager gameMan;

    public GameObject player;

    public float maxDist;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        gameMan.ReplacePlayerCosmetics();
        player.GetComponent<Player>().ChangeSpriteAlpha(Color.black, Color.clear);
        speed = speedReset;
        gameMan.ActivateCosmeticEffect();
        if (gameMan.cosEffect)
            Instantiate(gameMan.cosEffect);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position != new Vector3(0, 0, 0) && gamestart == true)
        {
            move = true;
            gamestart = false;
        }

        Vector3 playerY = new Vector3(0, player.transform.position.y, 0);
        Vector3 objY = new Vector3(0, gameObject.transform.position.y, 0);

        if (Vector3.Distance(objY, playerY) >= maxDist)
        {
            // Vector3 lockDist = new Vector3(gameObject.transform.position.x, player.transform.position.y - (maxDist - 0.5f));
            //gameObject.transform.position = lockDist;
            if (gameMan.canPlay == true)
                speed += 0.1f;
        }
        else if (speed > speedReset)
            speed -= 0.2f;

        if (gameMan.canPlay == true && move == true)
            MoveLava();
    }

    public void MoveLava()
    {
        float step = speed * Time.deltaTime;
        Vector3 newv1 = new Vector3(player.transform.position.x, player.transform.position.y + 3);
        transform.position = Vector3.MoveTowards(gameObject.transform.position, newv1, step);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameMan.FeedDataAndSave();
            gameMan.Fail();
        }
    }

    public void IncreaseSpeed(float speedupby)
    {
        speedReset += speedupby;
    }
}
