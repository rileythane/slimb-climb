﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheDestroyer : MonoBehaviour
{ 
    public void OnTriggerExit2D(Collider2D collision)
    {
       if(collision.gameObject.GetComponent<Tile>())
            collision.gameObject.SetActive(false);
    }    
}
