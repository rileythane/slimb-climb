﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highscores_UI : MonoBehaviour
{

    public List<int> highScores = new List<int>();
    public GameManager gameMan;

    // Use this for initialization
    void Start()
    {
        gameMan = GameObject.FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NewHighScore()
    {
        if (highScores.Count <= 5)
        {
            highScores.Add(gameMan.score);
        }

        else
        {
            for(int i = 0;i < highScores.Count; i++)
            {
                if(gameMan.score > highScores[i])
                {
                    highScores[i] = gameMan.score;
                }
            }
        }
    }
}


