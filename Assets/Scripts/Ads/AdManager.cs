﻿using System.Collections;
using UnityEngine;
using UnityEngine.Monetization;

public class AdManager : MonoBehaviour
{
    public static AdManager instance;

#if UNITY_IOS
       public static string gameId = "2926225";
#elif UNITY_ANDROID
    public string gameId = "2926223";
#endif
    bool testMode = false;

    public string placementId = "video";


    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if(instance == null)
        {
            instance = this;
        }
        else if(instance != null)
        {
            if(instance != this)
            {
                Destroy(this);
            }
        }
        else
        {
            //Debug.Log("You somehow broke this", this);
        }
    }
    void Start()
    {
        Monetization.Initialize(gameId, testMode);
    }
    public void ShowAdvert()
    {

        StartCoroutine(ShowAdWhenReady());
    }

    private IEnumerator ShowAdWhenReady()
    {
        while (!Monetization.IsReady(placementId))
        {
            yield return new WaitForSeconds(0.25f);
        }

        ShowAdPlacementContent ad = null;
        ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;

        if (ad != null)
        {
            ad.Show();
        }
    }
}
