﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestKey : MonoBehaviour
{
    public Chest chestMan;
    public GameManager gameMan;

    // Use this for initialization
    void Start()
    {
        chestMan = GameObject.FindGameObjectWithTag("ChestManager").GetComponent<Chest>();
        gameMan = GameObject.FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject.FindObjectOfType<AudioManager>().Play("KeyPickUp");
            chestMan.keys++;
            gameMan.chestKeys++;
            Destroy(gameObject);
        }
    }
}
