﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missions : MonoBehaviour
{
    #region SaveLoading Code
    [SerializeField] private GameConfig ReferenceConfig; // this links to the SO

    public GameConfig ActiveConfig;

   
    public void FeedDataAndSave()
    {

        ActiveConfig.savedIntergers.Clear();


        ActiveConfig.savedIntergers.Add(currentHeightMission);
        ActiveConfig.savedIntergers.Add(currentCoinMission);
        ActiveConfig.savedIntergers.Add(currentBoostMission);


        ActiveConfig.Save();
    }
    /// <summary>
    /// This is so bad but neeed to get this workinggsljfhaslkjdfhalkjs
    /// </summary>
    public void LoadAndPullData()
    {
        ActiveConfig.Load();
        if (ActiveConfig.savedIntergers.Count > 0)
        {
            int index = 0;
            currentHeightMission = ActiveConfig.savedIntergers[index];
            ++index;
            currentCoinMission = ActiveConfig.savedIntergers[index];
            ++index;
            currentBoostMission = ActiveConfig.savedIntergers[index];
           
        }

    }

    void Awake()
    {
        // clone the reference config so any changes won't overwrite
        ActiveConfig = ScriptableObject.Instantiate(ReferenceConfig);
  
        LoadAndPullData();
    }
    #endregion
   
    [System.Serializable]
    public class BasicMission
    {
        [Tooltip("Goal - how much X you need for the mission")]
        public int goal;

        [Tooltip("Current progress towards that goal")]
        public int progress;

        [Tooltip("Put the reward amount here")]
        public int coinReward;

        [Tooltip("Mission Complete")]
        public bool completed;
    }    

    public int currentHeightMission,
        currentCoinMission,
        currentBoostMission;

    public bool tester;

    public List<BasicMission> HeightMissions = new List<BasicMission>();
    public List<BasicMission> CoinMissions = new List<BasicMission>();
    public List<BasicMission> BoostMissions = new List<BasicMission>();

    GameManager gameMan;

    // Use this for initialization
    void Start()
    {
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        MaintainMissions();
        LoadAndPullData();
    }

    public void MaintainMissions()
    {
        currentHeightMission = gameMan.heightMission;
        currentCoinMission = gameMan.coinMission;
        currentBoostMission = gameMan.boostMission;

        HeightMissions[currentHeightMission].progress = gameMan.heightProgress;
        if (HeightMissions[currentHeightMission].progress >= HeightMissions[currentHeightMission].goal)
            HeightMissions[currentHeightMission].completed = true;

        CoinMissions[currentCoinMission].progress = gameMan.coinProgress;
        if (CoinMissions[currentCoinMission].progress >= CoinMissions[currentCoinMission].goal)
            CoinMissions[currentCoinMission].completed = true;

        BoostMissions[currentBoostMission].progress = gameMan.boostProgress;
        if (BoostMissions[currentBoostMission].progress >= BoostMissions[currentBoostMission].goal)
            BoostMissions[currentBoostMission].completed = true;
    }

    public void UpdateMissionProgress(ref int progress, int missionGoal, ref int currentMission, List<BasicMission> missionRef, ref bool turnOnOffProgress)
    {
        progress++;
        if(progress >= missionGoal)
        {
            progress = missionGoal;
            turnOnOffProgress = false;
            missionRef[currentMission].completed = true;
            /*gameMan.currency += missionReward;
            currentMission++;
            Debug.Log("No more");*/
        }
    }

    public void CheckMissionComplete(ref int progress, int missiongoal, ref int currentMissionMM, ref int currentMissionGM, List<BasicMission> missionRef, int missionReward, ref bool turnOnOffProgress)
    {
        if(progress >= missiongoal)
        {
            FeedDataAndSave();

            Rotate rot = FindObjectOfType<Rotate>();
            rot.CoinClick();
            progress = 0;
            turnOnOffProgress = true;
            gameMan.currency += missionReward;
            currentMissionMM++;
            if (currentMissionMM >= missionRef.Count)
            {
                currentMissionMM = 0;
            }
            currentMissionGM = currentMissionMM;
        }
        else
        {
            return;
        }
    }
}
